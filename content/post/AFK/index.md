+++
title = "AFK"
draft = false
date = "2015-03-11"
image = "betonvirtuel.png"
description = "AFK is a VR project offering an exploration experience in a liminal set up."
weight = "0"
+++

<!--more-->

###### screenshot of AFK, *Lucile Thierry game & level design, programmation, 3D*

*AFK* is an immersive virtual reality (VR) experience in which you put on a VR headset and explore a flooded city where all the buildings are closed and abandoned. The city is empty except for spots of light that guide players from one vista point to another. With *AFK*, I experimented with creating an atmospheric and engaging environment for VR devices through the use of narration (environmental only) and level design.

![screenshotAFK][1]
###### screenshot of AFK, *Lucile Thierry game & level design, programmation, 3D*
[1]: pistacheok1.png