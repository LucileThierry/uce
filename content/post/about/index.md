+++
title = "About & CV"
aliases = ["about-us", "about-hugo", "contact"]
author = "Lucile Thierry"
weight = 100
+++

*Looking for a 4 to 6 month internship, starting from March!*

<!--more-->

Hi, I'm UCE a game designer and artist. My mains interests are Science Fiction, Post-Apo and Utopia.
You will find in this site a selection of my works, latest to oldest.

Here's my [CV](/lucilethierryCV.pdf).
