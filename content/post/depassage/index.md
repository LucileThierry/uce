+++
showonlyimage = true
draft = false
image = "depassage.PNG"
date = "2020-07-20"
title = "De Passage"
description = "*De Passage* is a double production, both digital and physical."
+++

<!--more-->

###### de passage: exploration *video game by Marianne Vieulès: 3D, Vincent Kosellek: sounds, Lucile Thierry game & level design, programming*

*De Passage* is a double production, both digital and physical, conceived by three artists, in a period of health crisis. Both an exhibition and a game, *De passage* casts a misty eye on the hypotheses of societal collapse.

*De passage* is an evocation of a mad spirit, that of the architect who slumbers in each of us and who dreams of a human permanence that has become impossible. This mad spirit, against all odds, develops a utopian form to accommodate its aspiration. *De passage* is a double visit, a journey to the border of incompatible eras.

*De passage: Exploration* is the digital interface to which you are invited on itch.io: A traveller treads the synthetic ground of an artificial hill. In front of him, a suspended space. The only sound of the city he sees is the song of thousands of birds. Streetlights faintly pierce the sea mist. An invitation to discover, their light draws a path. *Exploration* invites the visitor-traveller to explore the city of Lierre (Ivy) in a digital space, between ruin and construction site, at a time when the discreet presence of the Architect is still felt. Satellite remnants, mobile lampposts and heliotropic buildings are all archaeological remains of a dislocated history.

*De passage: Appartement* is the immersive installation developed at the art centre Chantier Public in Poitiers: Appartement is situated in a more distant, even more unknown time. Chantier Public opens up to visitors and reveals a dwelling, a quasi-museal space that reveals the last remnants of the Architect's life and his project. Outside, all that remains, like all traces of a humanity that has become myth, are strange missives with no real addressees, lost letters carried by the ultimate couriers that birds have become. 


![Extract of de passage][1]
###### de passage: appartement *installation Marianne Vieulès, Vincent Kosellek, Lucile Thierry*
[1]: appartement1.jpg