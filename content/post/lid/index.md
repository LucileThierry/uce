+++
showonlyimage = true
draft = false
image = ""
date = "2022-11-05"
title = "Lost in depiction"
weight = 0
+++

<!--more-->

As you walk home, by night, you’re drawn to the lights and noise of an art gallery. Inside, you discover a strange and mysterious venue. You soon realise that touching a painting transports you into the depicted scene.
In *Lost in Depiction*, play as Everest as she explores these pictorial spaces and unravels the underlying mysteries concealed by the gallery.

