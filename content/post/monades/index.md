+++
showonlyimage = true
draft = false
image = "monades1.jpg"
date = "2017-11-05"
title = "monades"
weight = 0
+++

<!--more-->

###### Monades 1 *Lucile Thierry, photography, 120x60 cm, 2017*

*Monades* is a series of six photographs. Each one shows a different arrangement of files on a hard drive.

The point was to create landscapes with non-pictorial information: files on a hard drive. Traditionally, pictorial landscapes have some rules, such as the land/sky proportion must respect a 1/3-2/3 ratio.

In *monades*, files take the place of skies, seas and buildings.

![monades 2][1]
###### Monades 2 *Lucile Thierry, photography, 120x60 cm, 2017*
[1]: monades2.jpg