+++
showonlyimage = true
draft = false
image = "thelastsunrise.jpg"
date = "2015-06-05"
title = "the last sunrise"
weight = 0
+++

<!--more-->

###### Frame of *The last sunrise*, installation, video, video projector, loungers, tequila sunrise, bossa nova, Lucile Thierry & Clément Lévignat 

The last sunrise set up a slow-motion video recording of a nuclear test, with bossa nova playing in the background. Spectators take a seat in one of the two sun loungers, and glasses of tequila sunrise are available.

Pushed to its farthest extent, the slow-motion show abstracts and aestheticizes the images of the nuclear explosion. The cynical behavior of the piece is quickly replaced by visual and immersive chaos, to the point where we forget its origin until the end.

Therefore, the purpose here is multifaceted, leading the spectator through numerous sensations, from cynicism to amazement and finally, drunkenness.

![Extract of the last sunrise][1]
###### Photography of *The last sunrise*, installation, video, video projector, loungers, tequila sunrise, bossa nova, Lucile Thierry & Clément Lévignat 
[1]: thelastsunriseinsitu.jpg