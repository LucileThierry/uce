+++
title = "VOID"
draft = false
date = "2017-09-11"
image = "plage1.jpg"
weight = "0"
+++

<!--more-->

###### Screenshot of *The Beach* VOID, Everyware, 2017, AR (augmented reality), Nicolas Leray game & level design, programming, Clément Sipion game & level design, arts, Lucile Thierry game & level design, arts

In *VOID* we spatialized science fiction comic strip vignettes in a projected space within tablets. While questioning the inter-iconic space and the formation of a hypertextual comic strip.

In this device, spectators are led through tablets to observe and move through virtual spaces composed from the universe of the proposed comic strip. Each mobile device is fixed to the ceiling by removable plastic hooks allowing the orientation of the tablet to be changed and thus the point of view in the virtual space. The user is prompted to press various elements that make up the space to move from one vignette to another (in a hypertextual narrative way). The chosen environments are digitized and then transposed into a three-dimensional space, by deconstructing and reconstructing the planes in space.

![Extract of VOID][2]
###### Screenshot of *Saturn* VOID, Everyware, 2017, AR (augmented reality), Nicolas Leray game & level design, programming, Clément Sipion game & level design, arts, Lucile Thierry game & level design, arts
[2]: 48.png